/* 
1. DOM - структура документа, де все, що знаходиться на сторінці, подається як об'єкт, і за допомогою якого можна змінювати сторінку.

2. innerHTML - виводить текстову складову з внутрішніми тегами (br, i, strong, span і тд.).
   innerText - виводить тільки текстову складову, опускаючи всі теги.

   3. Існують такі методи:

   document.querySelector - пошук по будь-якому CSS-селектору, повертає один об'єкт, жива колекція - НІ, пошук всередині елементу - ТАК.

   document.querySelectorAll - пошук по будь-якому CSS-селектору, повертає колекцію об'єктів, жива колекція - НІ, пошук всередині елементу - ТАК.

   document.getElementById - пошук по ID, повертає один об'єкт, жива колекція - НІ, пошук всередині елементу - НІ.

   document.getElementByTagName - пошук по тегу, повертає колекцію об'єктів, жива колекція - ТАК, пошук всередині елементу - ТАК.

   document.getElementByClassName - пошук по класу, повертає колекцію об'єктів, жива колекція - ТАК, пошук всередині елементу - ТАК.

   document.getElementByName - пошук по NAME, повертає колекцію об'єктів, жива колекція - ТАК, пошук всередині елементу - НІ.
*/



// 1)
// const paragraphs = document.querySelectorAll('p')
// for (const item of paragraphs) {
//   item.style.background = '#ff0000'
// }



// 2)
// const optionsList = document.getElementById('optionsList')
// console.log(optionsList)

// const parentOptionsList = optionsList.parentElement
// console.log(parentOptionsList)

// if (!!optionsList.children) {
//   for (const node of optionsList.children) {
//     console.log('Повна нода -', node)
//     console.log('Ім"я ноди -', node.nodeName)
//     console.log('Тип ноди -', node.nodeType)
//   }
// } else {
//   console.log('Дочірніх нод немає')
// }



// 3) 
// const newParagraph = document.createElement('p')

// const newParagraphClass = newParagraph.classList = 'testParagraph'

// const newParagraphTextContent = newParagraph.textContent = 'This is a paragraph'

// document.body.insertAdjacentHTML('afterbegin', `<p class=${newParagraphClass}>${newParagraphTextContent}</p>`)



// 4) 
// const elements = document.querySelectorAll('.main-header li')
// console.log(elements)

// for (item of elements) {
//   item.classList = 'nav-item'
// }

// console.log(elements)



// 5)
// const elements = document.querySelectorAll('.section-title')

// for (element of elements) {
//   element.classList.remove('section-title')
// }

// console.log(elements)
